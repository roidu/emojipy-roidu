# -*- coding: utf-8; -*-

from setuptools import setup

setup(name='emojipy-roidu',
      version='3.0.6',
      description='Python wrapper for emojione',
      packages=['emojipy',],
      test_suite='emojipy.tests',
      keywords = ['emojipy', 'emojione', 'python emoji'],
)
