# import cog
import json

json_package_path = 'emoji.json'

data_dict = json.loads(open(json_package_path).read())

unicode_replace = {}
shortcode_replace = {}
ascii_replace = {}
category_replace = {}

for key, value in data_dict.items():
    unicode_hex = value['code_points']['base']
    ascii = value['ascii']
    shortname = value['shortname']

    # Don't replace digits
    if 'digit_' in shortname:
        continue

    if ascii:
        for item in ascii:
            ascii_replace[item] = unicode_hex
    shortcode_replace[shortname] = unicode_hex
    category_replace[shortname] = value['category']


    if '-' not in unicode_hex:
        unicode_char = chr(int(unicode_hex, 16))
        unicode_replace[unicode_char.encode('utf-8')] = shortname
    else:
        parts = unicode_hex.split('-')
        unicode_char = ''.join(chr(int(part, 16)) for part in parts)
        unicode_replace[unicode_char.encode('utf-8')] = shortname

print('unicode_replace = %s\n\n' % unicode_replace)
print('shortcode_replace = %s\n\n' % shortcode_replace)
print('ascii_replace = %s\n\n' % ascii_replace)
print('category_replace = %s\n\n' % category_replace)
